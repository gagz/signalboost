// TODO: move statuses to util and kill this file!
const statuses = {
  SUCCESS: 'SUCCESS',
  NOOP: 'NOOP',
  ERROR: 'ERROR',
}

// TODO: move languages to dispatcher/strings/index and kill this file!
const languages = {
  EN: 'EN',
  ES: 'ES',
  FR: 'FR',
}

module.exports = { statuses, languages }
