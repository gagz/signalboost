const { upperCase } = require('lodash')
const { memberTypes } = require('../../../../db/repositories/membership')
const {
  getAdminMemberships,
  getSubscriberMemberships,
} = require('../../../../db/repositories/channel')

const systemName = 'l\'administrateurice du système Signalboost'
const systemName = 'le maintenant du système Signalboost'
const notAdmin =
  'Déso, seul·e·s les admins sont autorisé·es à exécuter cette commande. Envoyez AIDE pour une liste de commandes valides.'
const notSubscriber =
  "Votre commande n'a pas pu être traitée car vous n'êtes pas abonné·e à ce canal. Envoyez ALLO pour vous abonner."

const invalidNumber = phoneNumber =>
  `Oups! "${phoneNumber}" n’est pas un numéro de téléphone valide. Les numéros de téléphone doivent comprendre le code pays précédé par un «+».`

const support = `----------------------------
COMMENT ÇA FONCTIONNE
----------------------------

Un canal Signalboost a des administrateurices et des abonné·e·s.

-> Lorsque les administratrices-teurs envoient des messages, ces messages sont envoyés à l'ensemble des abonné·e·s.
-> Si activé, les abonné·e·s peuvent envoyer des réponses que seul·e·s les administrateurices peuvent lire.
-> Les abonné·e·s ne peuvent pas envoyer des messages entre elleux. (Pas de cacophonie!)

Un canal Signalboost comprend un certain nombre de commandes.

-> Envoyer AIDE retourne la liste des commandes.
-> On peut s'abonner en utilisant la commande ALLO, ou se désabonner avec ADIEU.
-> Envoyer le nom d’une langue (par exemple: ESPAÑOL ou ANGLAIS) changera la langue.

Signalboost tente de préserver votre intimité.

-> Les utilisateurices ne peuvent pas voir les numéros de téléphone des autres membres.
-> Signalboost ne lit et ne conserve aucun des messages.

Pour plus de renseignements: https://signalboost.info`

const notifications = {
  adminAdded: (commandIssuer, addedAdmin) =>
    `Nouvel·le admin ${addedAdmin} ajouté·e par ${commandIssuer}`,

  hotlineMessageSent: channel =>
    `Votre message a été transmis de manière anonyme aux admins de [${
      channel.name
    }]. Indiquez votre numéro de téléphone si vous souhaitez que les administrateuricess vous répondent individuellement.'

Envoyez AIDE pour voir la liste des commandes que je comprends.`,

  hotlineMessagesDisabled: isSubscriber =>
    isSubscriber
      ? 'Déso, les messages entrants ne sont pas activés sur ce canal. Envoyez AIDE pour répertorier les commandes valides.'
      : 'Déso, les messages entrants ne sont pas activés sur ce canal. Envoyez AIDE pour lister les commandes valides ou ALLO pour vous abonner.',

  deauthorization: adminPhoneNumber => `
${adminPhoneNumber} a été retiré·e de ce canal parce que leur numéro de sécurité a changé.

Ceci est presque certainement parce qu'ielles ont réinstallé Signal sur un nouvel appareil.

Cependant, il y a un petit risque que leur téléphone soit compromis et qu'une autre personne tente de se faire passer pour elleux.

Vérifiez auprès de ${adminPhoneNumber} que leur appareil est toujours sous leur controle. Vous pouvez par la suite les réajouter en envoyant:

AJOUTER ${adminPhoneNumber}

Ielles ne seront pas autorisé·e·s à envoyer ou à lire des messages sur ce canal avant que cette étape soit complétée.`,
  noop: 'Oups! Ceci n’est pas une commande!',
  unauthorized:
    'Oups! Je n\'ai pas compris.\nEnvoyez AIDE pour voir la liste des commandes que je comprends!',

  welcome: (addingAdmin, channelPhoneNumber) => `
Vous êtes maintenant un·e
 admin de ce canal Signalboost grâce à ${addingAdmin}. Bienvenue!

On peut aussi s’abonner à ce canal en envoyant la commande ALLO au ${channelPhoneNumber}, et se désabonner avec la commande ADIEU.

Envoyez AIDE pour plus de renseignements.`,

  signupRequestReceived: (senderNumber, requestMsg) =>
    `Demande d’abonnement reçue du ${senderNumber}:\n ${requestMsg}`,

  signupRequestResponse:
    'Merci pour votre inscription à Signalboost! Vous recevrez bientôt un message d\'accueil sur votre nouveau canal…',
}

const commandResponses = {
  // ADD

  add: {
    success: num => `${num} ajouté·e comme admin.`,
    notAdmin,
    dbError: num =>
      `Oups! Une erreur s’est produite en tentant de supprimer ${num}. Veuillez essayer de nouveau.`,
    invalidNumber,
  },

  // REMOVE

  remove: {
    success: num => `${num} supprimé·e des admins.`,
    notAdmin,
    dbError: num =>
      `Oups! Une erreur s'est produite lors de la tentative de suppression ${num}. Veuillez essayer de nouveau.`,
    invalidNumber,
    targetNotAdmin: num => `Oups! ${num} n’est pas un·e admin. Iel ne peut être supprimé·e des admins.`,
  },

  // HELP

  help: {
    admin: `----------------------------------------------
COMMANDES QUE JE COMPRENDS
----------------------------------------------

AIDE
-> liste les commandes

INFO
-> affiche les stats, explique le fonctionnement de Signalboost

RENOMMER nouveau nom
-> renomme le canal en "nouveau nom"

AJOUTER +1-555-555-5555
-> ajoute +1-555-555-5555 comme admin

SUPPRIMER +1-555-555-5555
-> supprime +1-555-555-5555 des admins

RÉPONSES ACTIVÉES
-> permet aux abonné·e·s d’envoyer des messages aux admins

RÉPONSES DÉSACTIVÉES
-> désactive la possibilité des abonné·e·s d’envoyer des messages aux admins

ADIEU
-> se désabonner du canal 

ESPAÑOL / ENGLISH
-> change la langue en espagnol ou anglais`,

    subscriber: `----------------------------------------------
COMMANDES QUE JE COMPRENDS
----------------------------------------------

AIDE
-> liste les commandes

INFO
-> affiche les stats, explique le fonctionnement de Signalboost

ALLO
-> abonnement au canal

ADIEU
-> désabonnement du canal

ESPAÑOL / ENGLISH
-> change la langue en espagnol ou anglais`,
  },

  // INFO

  info: {
    [memberTypes.ADMIN]: channel => `---------------------------
INFOS CANAL
---------------------------

Vous êtes admin de ce canal.

nom: ${channel.name}
numéro de téléphone: ${channel.phoneNumber}
admins: ${getAdminMemberships(channel).length}
abonné·e·s: ${getSubscriberMemberships(channel).length}
réponses: ${channel.responsesEnabled ? 'ON' : 'OFF'}
messages envoyés: ${channel.messageCount.broadcastIn}

${support}`,

    [memberTypes.SUBSCRIBER]: channel => `---------------------------
INFOS CANAL
---------------------------

Vous êtes abonné·e à ce canal.

nom: ${channel.name}
numéro de téléphone: ${channel.phoneNumber}
réponses: ${channel.responsesEnabled ? 'ON' : 'OFF'}
abonné·e·s: ${getSubscriberMemberships(channel).length}

${support}`,

    [memberTypes.NONE]: channel => `---------------------------
INFOS CANAL
---------------------------

Vous n'êtes pas abonné à cette chaîne. Envoyez AIDE pour vous abonner.

nom: ${channel.name}
numéro de téléphone: ${channel.phoneNumber}
abonnées: ${getSubscriberMemberships(channel).length}

${support}`,
  },

  // RENAME

  rename: {
    success: (oldName, newName) => `[${newName}]\nNom du canal changé de "${oldName}" à "${newName}”.`,
    dbError: (oldName, newName) =>
      `[${oldName}]\nOups! Une erreur s’est produite en tentant de renommer le canal de [${oldName}] à [${newName}]. Veuillez essayer de nouveau!`,
    notAdmin,
  },

  // JOIN

  join: {
    success: channel =>
      `Bienvenue à Signalboost! Vous êtes maintenant abonné·e au canal [${channel.name}].

Répondez avec AIDE pour en savoir plus ou ADIEU pour vous désinscrire.`,
    dbError: `Oups! Une erreur s’est produite en tentant de vous ajouter au canal. Veuillez essayer de nouveau!`,
    alreadyMember: `Oups! Vous êtes déjà abonné·e à ce canal.`,
  },

  // LEAVE

  leave: {
    success: `Vous êtes maintenant désabonné·e de ce canal. Au revoir!`,
    error: `Oups! Une erreur s’est produite en tentant de vous désabonner de ce canal. Veuillez essayer de nouveau!`,
    notSubscriber,
  },

  // RESPONSES_ON / RESPONSES_OFF

  toggleResponses: {
    success: setting => `Réponses des abonnées passées à ${upperCase(setting)}.`,
    notAdmin,
    dbError: setting =>
      `Oups! Une erreur s’est produite en tentant de changer les réponses à ${setting}. Veuillez essayer de nouveau!`,
  },

  // SET_LANGUAGE

  setLanguage: {
    success: `Je vous parlerai maintenant en français!
    
Commande AIDE pour la liste des commandes que je comprends.`,
    dbError: 'Oups! La langue n’a pas été changée. Veuillez essayer de nouveau!',
  },

  // TRUST

  trust: {
    success: phoneNumber => `Mise à jour du numéro de sécurité de ${phoneNumber}`,
    error: phoneNumber =>
      `La mise à jour du numéro de sécurité de ${phoneNumber} a échoué. Veuillez essayer à nouveau ou contacter un·e mainteneur·euse!`,
    invalidNumber,
    notAdmin,
    dbError: phoneNumber =>
      `Oups! Une erreur s’est produite lors de la mise à jour du numéro de sécurité de ${phoneNumber}. Veuillez essayer à nouveau!`,
  },
}

const prefixes = {
  hotlineMessage: `RÉPONSES ABONNÉ·E·S`,
}

module.exports = {
  commandResponses,
  notifications,
  prefixes,
  systemName,
}
