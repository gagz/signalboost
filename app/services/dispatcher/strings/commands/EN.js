module.exports = {
  ADD: ['ADD'],
  HELP: ['HELP'],
  INFO: ['INFO'],
  JOIN: ['HELLO', 'JOIN'], // we recognize "JOIN" and "LEAVE" for backwards compatibility
  LEAVE: ['GOODBYE', 'LEAVE'],
  REMOVE: ['REMOVE'],
  RENAME: ['RENAME'],
  RESPONSES_ON: ['RESPONSES ON'],
  RESPONSES_OFF: ['RESPONSES OFF'],
  SET_LANGUAGE: ['ENGLISH', 'INGLÉS', 'INGLES', 'ANGLAIS'],
}
